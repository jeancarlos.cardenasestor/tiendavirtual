package main;

public class Laptop extends ProductoElectronico {
	private String tipodealmacenamiento;

	public String getTipodealmacenamiento() {
		return tipodealmacenamiento;
	}

	public void setTipodealmacenamiento(String tipodealmacenamiento) {
		this.tipodealmacenamiento = tipodealmacenamiento;
	}

	@Override
	public String toString() {
		return "ProductoElectronico [marca = " + marca + ", modelo = " + modelo + ", precio = " + precio + ", tipo de almacenamiento ="+tipodealmacenamiento+"]";
	}


	

}
