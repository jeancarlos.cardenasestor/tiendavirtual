package main;

public class ProductoElectronico {
	
	protected String marca;
	protected String modelo;
	protected double precio;

	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	@Override
	public String toString() {
		return "ProductoElectronico [marca = " + marca + ", modelo = " + modelo + ", precio = " + precio + "]";
	}

	
	
}
