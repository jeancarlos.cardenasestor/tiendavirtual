package main;

public class Televisores extends ProductoElectronico{
	
	private int pulgadas;

	public int getPulgadas() {
		return pulgadas;
	}

	public void setPulgadas(int pulgadas) {
		this.pulgadas = pulgadas;
	}

	@Override
	public String toString() {
		return "ProductoElectronico [marca = " + marca + ", modelo = " + modelo + ", precio = " + precio + ", pulgadas=" + pulgadas +"]";
	}



}
