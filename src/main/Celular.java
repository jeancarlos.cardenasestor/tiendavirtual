package main;

public class Celular extends ProductoElectronico {
	private String pixelescamarafrontal;

	public String getPixelescamarafrontal() {
		return pixelescamarafrontal;
	}

	public void setPixelescamarafrontal(String pixelescamarafrontal) {
		this.pixelescamarafrontal = pixelescamarafrontal;
	}

	
	public String toString() {
		return "ProductoElectronico [marca = " + marca + ", modelo = " + modelo + ", precio = " + precio + ",pixelescamara frontal ="+ pixelescamarafrontal +"]";
	}


}
