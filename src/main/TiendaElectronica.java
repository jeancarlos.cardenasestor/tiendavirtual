package main;

import java.util.ArrayList;
import java.util.List;


public class TiendaElectronica {
	 private List<String> productos;
	    private List<String> carrito;

	    public TiendaElectronica() {
	        productos = new ArrayList<>();
	        carrito = new ArrayList<>();
	    }

	    public void agregarProducto(String producto) {
	        productos.add(producto);
	    }

	    public void mostrarProductos() {
	        System.out.println("Productos disponibles:");
	        for (int i = 0; i < productos.size(); i++) {
	            System.out.println((i+1) + ". " + productos.get(i));
	        }
	    }

	    public void agregarAlCarrito(int indice) {
	        if (indice < 1 || indice > productos.size()) {
	            System.out.println("Indice inválido.");
	            return;
	        }
	        carrito.add(productos.get(indice-1));
	        System.out.println("Producto agregado al carrito.");
	    }

	    public void verCarrito() {
	        System.out.println("Productos en el carrito:");
	        for (int i = 0; i < carrito.size(); i++) {
	            System.out.println((i+1) + ". " + carrito.get(i));
	        }
	    }

	    public void eliminarDelCarrito(int indice) {
	        if (indice < 1 || indice > carrito.size()) {
	            System.out.println("Indice inválido.");
	            return;
	        }
	        carrito.remove(indice-1);
	        System.out.println("Producto eliminado del carrito.");
	    }

	    public void finalizarCompra() {
	        System.out.println("Compra finalizada. Productos comprados:");
	        for (String producto : carrito) {
	            System.out.println("- " + producto);
	        }
	        carrito.clear();
	    }
	    
}
	


